/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import exceptionHandler.exceptions.UnregisteredObserverException;
import events.Observable;
import events.Observer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import model.units.Unit;
import events.EventStorage;
import java.util.Map;
import log.Log;

/**
 *
 * @author miha
 */
public abstract class AbstractController implements Observable {

    private HashMap<String, ArrayList<Observer>> events;

    public AbstractController() {
        events = EventStorage.getObservers();
    }

    @Override
    public void registerObserver(String event, Observer observer) {
        if (events.containsKey(event)) {
            events.get(event).add(observer);
        } else {
            ArrayList<Observer> observerList = new ArrayList<Observer>();
            observerList.add(observer);
            events.put(event, observerList);
        }
    }

    @Override
    public void removeObserver(String event, Observer observer) {
        if (events.containsKey(event)) {
            ArrayList<Observer> observerList = events.get(event);
            if (observerList.contains(observer)) {
                observerList.remove(observer);
            } else {
                throw new UnregisteredObserverException(event, "Event with name '" + event + "' is empty.");
            }
        } else {
            throw new UnregisteredObserverException(event);
        }
    }

    @Override
    public void removeEvent(String event) {
        if (events.containsKey(event)) {
            events.remove(event);
        } else {
            throw new UnregisteredObserverException(event);
        }
    }

    @Override
    public void notifyObservers(String event, Map<String, Object> params) {
        if (events.containsKey(event)) {
            ArrayList<Observer> eventsList = events.get(event);
            if (!eventsList.isEmpty()) {
                for (Observer observer : eventsList) {
                    observer.update(params);
                }
            }
            logIt(event, params);
        }
    }

    private void logIt(String event, Map<String, Object> params) {
        if (Log.isDebugMode()) {
            StringBuilder logString = new StringBuilder("");
            if (params != null) {
                for (String paramName : params.keySet()) {
                    Object paramValue = params.get(paramName);
                    if (paramValue != null) {
                        logString.append(paramName).append(" : ").append(paramValue.toString()).append("\n");
                    }
                }
            }
            Log.add(event + "\n" + params);
        }
    }

    public void notifyObservers(String event, String paramName, Object object) {
        Map<String, Object> map = new HashMap<String, Object>();
        map.put(paramName, object);
        notifyObservers(event, map);
    }

    /**
     * Perform controllers action
     *
     * @return working status
     */
    public abstract int run();
}
