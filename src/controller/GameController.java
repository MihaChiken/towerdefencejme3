/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import events.EventStorage;
import exceptionHandler.ExceptionHandler;
import controller.initializer.Initializer;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import model.Level;
import model.NodeChain;
import model.Point;
import model.User;
import model.towers.*;
import model.units.*;
import events.Observer;
import events.controller.TowerCreateObserver;
import events.controller.TowerHitObserver;
import events.controller.TowerReadyObserver;
import events.controller.TowerRemoveObserver;
import events.controller.UnitDeathObserver;
import events.controller.UnitFinishedObserver;

/**
 * Initialize level and start other controllers
 *
 * @author miha
 */
public class GameController extends AbstractController {
    //Constants

    public static final int STATUS_ERROR = 3;
    public static final int STATUS_PLAYER_WIN = 2;
    public static final int STATUS_PLAYER_LOOSE = 1;
    public static final int STATUS_NOTHING_HAPPENS = 0;
    private static GameController instance = null;
    private Level level;
    private User user;
    private UnitsController unitsController;
    private TowersController towersController;

    public Level getLevel() {
        return level;
    }

    private GameController(Initializer initializer) {

        //Initializing variables
        level = new Level();

        unitsController = new UnitsController(new UnitSequence(), level.getNodeChain());
        towersController = new TowersController(new TowersMap());
        
         //Running dynamic initializer
        initializer.init(towersController.getTowersMap(), unitsController.getUnitSequence(), level);
        
        user = new User("User1", level.getStartGoldAmount(), level.getLifes());

        //Initializing observers
        registerObserver(EventStorage.UNIT_DEATH, new UnitDeathObserver(user));
        registerObserver(EventStorage.UNIT_FINISHED, new UnitFinishedObserver(user));
        registerObserver(EventStorage.TOWER_READY, new TowerReadyObserver(unitsController.getUnitSequence()));
        registerObserver(EventStorage.TOWER_HIT, new TowerHitObserver(unitsController.getUnitSequence())); 
        registerObserver(EventStorage.TOWER_CREATE, new TowerCreateObserver(user));  
        registerObserver(EventStorage.TOWER_REMOVE, new TowerRemoveObserver(user));  
        
        notifyObservers(EventStorage.GAME_START, "Level", level);
    }

    public static GameController init(Initializer initializer) {
        if (instance == null) {
            instance = new GameController(initializer);
        }
        return instance;
    }

    public static GameController initTestMode(Initializer initializer) {

        return new GameController(initializer);
    }

    @Override
    public int run() {
        //Running controllers
        int status;
//        try {
            towersController.run();
            status = unitsController.run();
            notifyObservers(EventStorage.GAME_CONTROLLER_RUN, null);
//        } catch (Exception e) {
//            ExceptionHandler.catchIt(e);
//            status = STATUS_ERROR;
//        }
        //Returning response based on status
        if (status == UnitsController.USER_LOST_A_LIFE && user.getLifes() == 0) {
            return STATUS_PLAYER_LOOSE;
        }
        if (status == UnitsController.USER_WIN) {
            return STATUS_PLAYER_WIN;
        }
        return STATUS_NOTHING_HAPPENS;
    }

    public UnitsController getUnitsController() {
        return unitsController;
    }

    public TowersController getTowersController() {
        return towersController;
    }

    public User getUser() {
        return user;
    }
}
