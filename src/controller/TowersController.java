/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import events.EventStorage;
import events.controller.TowerCreateObserver;
import events.controller.TowerHitObserver;
import java.util.List;
import model.Point;
import model.towers.SimpleTower;
import model.towers.Tower;
import model.towers.TowersMap;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;
import events.controller.TowerReadyObserver;
import java.util.HashMap;
import java.util.Map;
import model.towers.AbstractBattleTower;
import model.towers.ShootableTower;

/**
 * Controll towers shooting, adding, removing
 *
 * @author miha
 */
public class TowersController extends AbstractController {

    private TowersMap towersMap;

    public TowersController(TowersMap towersMap) {
        this.towersMap = towersMap;
    }

    public TowersMap getTowersMap() {
        return towersMap;
    }

    /**
     * @param tower
     * @param goldAmount
     * @return is it poosible to build the tower
     */
    public boolean checkTowerBuilding(Tower tower, int goldAmount) {
        return goldAmount >= tower.getCost() && towersMap.isFreeSpace(tower.getPoint());
    }

    @Override
    public int run() {
        List<Tower> towers = towersMap.getTowers();
        for (Tower tower : towers) {
            if (tower instanceof ShootableTower) {
                shootingTowerLoop((AbstractBattleTower)tower);
            }
            tower.runTowerLoop();
        }
        return 0;
    }

    public void addTower(Tower tower) {
        towersMap.getTowers().add(tower);

        notifyObservers(EventStorage.TOWER_CREATE, TowerCreateObserver.PARAM_TOWER, tower);
    }

    public void addTower(float x, float y) {
        Tower tower = new SimpleTower(new Point(x, y));
        addTower(tower);
    }

    public void removeTower(Tower tower) {
        if (towersMap.getTowers().contains(tower)) {
            towersMap.getTowers().remove(tower);
            notifyObservers(EventStorage.TOWER_REMOVE, "tower", tower);
        }
    }

    private void shootingTowerLoop(AbstractBattleTower battleTower) {
        
        shooting(battleTower);
        //Check hiting
        Unit unit = battleTower.getHittedUnit();
        if (unit != null) {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(TowerHitObserver.PARAMS_TOWER, battleTower);
            params.put(TowerHitObserver.PARAMS_UNIT, unit);

            notifyObservers(EventStorage.TOWER_HIT, params);
        }
    }

    private void shooting(ShootableTower tower) {
        if (tower.isTowerReady()) {
            Unit unit = null;
            Map<String, Object> params = new HashMap<String, Object>();
            params.put(TowerReadyObserver.PARAMS_TOWER, tower);
            params.put(TowerReadyObserver.PARAMS_UNIT, unit);

            notifyObservers(EventStorage.TOWER_READY, params);
            if (params.get(TowerReadyObserver.PARAMS_UNIT) != null) {
                notifyObservers(EventStorage.TOWER_SHOT, params);
            }
        }
    }
}
