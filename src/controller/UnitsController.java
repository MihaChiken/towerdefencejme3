/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import events.EventStorage;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import model.NodeChain;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;
import events.controller.UnitDeathObserver;
import events.controller.UnitFinishedObserver;

/**
 * Controll unit moving, dying and getting next waves
 *
 * @author miha
 */
public class UnitsController extends AbstractController {

    public static int USER_WIN = 2;
    public static int USER_LOST_A_LIFE = 1;
    public static int NOTHING_HAPPENS = 0;
    private UnitSequence unitSequence;
    private NodeChain nodeChain;

    public UnitsController(UnitSequence unitSequence,
            NodeChain nodeChain) {

        this.unitSequence = unitSequence;
        this.nodeChain = nodeChain;
    }

    public UnitSequence getUnitSequence() {
        return unitSequence;
    }
    // TODO: refactor this method
    // Warning: bad code

    @Override
    public int run() {
        List<UnitGroup> groups;
        groups = unitSequence.getUnitsGroupsOnMap();
        // Starting new wave if previous is dead
        if (groups.isEmpty()) {
            if (!getNextWave(groups)) {
                //Waves ended
                return USER_WIN;
            }
        }
        for (UnitGroup group : groups) {
            List<Unit> units = group.getUnits();
            Iterator<Unit> unitIt = units.iterator();
            while (unitIt.hasNext()) {
                //Recieving next unit
                Unit unit = unitIt.next();

                notifyObservers(EventStorage.UNIT_MOVE, "unit", unit);
                int unitStatus = unit.move(nodeChain);

                if (unitStatus == Unit.DEAD) {
                    unitIt.remove();
                    notifyObservers(EventStorage.UNIT_DEATH, UnitDeathObserver.PARAMS_NAME, unit);
                }

                if (unitStatus == Unit.FINISHED) {
                    unitIt.remove();
                    notifyObservers(EventStorage.UNIT_FINISHED, "unit", unit);
                    return USER_LOST_A_LIFE;
                }
            }
            //Removing empty(dead) groups
            if (group.isEmpty()) {
                unitSequence.getUnitsGroups().remove(group);
            }

        }
        return NOTHING_HAPPENS;
    }

    private boolean getNextWave(List<UnitGroup> groups) {
        UnitGroup group = unitSequence.nextGroup();
        if (group != null) {
            groups.add(group);
            return true;
        } else {
            //Waves ended
            return false;
        }
    }
}
