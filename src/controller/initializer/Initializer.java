/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.initializer;

import model.Level;
import model.NodeChain;
import model.towers.TowersMap;
import model.units.UnitSequence;

/**
 *
 * @author miha
 */
public interface Initializer {

    public void init(TowersMap towersMap, UnitSequence unitSequence, Level level);
}
