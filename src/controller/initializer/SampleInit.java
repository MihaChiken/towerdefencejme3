/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.initializer;

import controller.initializer.Initializer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import model.Level;
import model.NodeChain;
import model.Point;
import model.towers.SimpleTower;
import model.towers.TowersMap;
import model.units.SimpleUnit;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 *
 * @author miha
 */
public class SampleInit implements Initializer {

    @Override
    public void init(TowersMap towersMap, UnitSequence unitSequence, Level level) {
//        towersMap.getTowers().add(new SimpleTower(new Point(1, 82)));
//        towersMap.getTowers().add(new SimpleTower(new Point(40, 82)));
//        towersMap.getTowers().add(new SimpleTower(new Point(70, 82)));


        unitSequence.getUnitsGroups().add(new UnitGroup(true, createWave(6,7,0)));
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(9,6,0)));
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(9,6,1)));
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(9,6,2)));
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(10,5,3)));
        
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(5,8,6)));
        
        unitSequence.getUnitsGroups().add(new UnitGroup(false, createWave(3,3,10)));

        NodeChain nodeChain = level.getNodeChain();
        nodeChain.addNode(new Point(0, 80));
        nodeChain.addNode(new Point(70, 80));
        nodeChain.addNode(new Point(70, 60));
        nodeChain.addNode(new Point(30, 60));
        nodeChain.addNode(new Point(30, 40));

        level.setStartGoldAmount(179);
        level.setLifes(3);
    }

    private List<Unit> createWave(int amount, int rangeBeetwenUnits, int level) {
        List<Unit> wave = new ArrayList<Unit>();
        for (int i = 0; i < amount; i++) {
            wave.add(new SimpleUnit(new Point(-i*rangeBeetwenUnits, 80), level));
        }
        return wave;
    }
}
