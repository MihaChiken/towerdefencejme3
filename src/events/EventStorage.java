/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Event storage consist all events registered by view and controller too
 *
 * @author miha
 */
public class EventStorage {
    public static final String GAME_START = "GameStart";
    public static final String GAME_CONTROLLER_RUN = "GameControllerRun";
    
    public static final String TOWER_SHOT = "TowerShot";
    public static final String TOWER_HIT = "TowerHit";
    public static final String TOWER_READY = "TowerReady";
    public static final String TOWER_CREATE = "TowerCreate";
    public static final String TOWER_REMOVE = "TowerRemove";
    
    public static final String UNIT_DEATH = "UnitDeath";
    public static final String UNIT_FINISHED = "UnitFinished";
    public static final String UNIT_MOVE = "UnitMove";
    private static HashMap<String, ArrayList<Observer>> observers = new HashMap<String, ArrayList<Observer>>();

    public static HashMap<String, ArrayList<Observer>> getObservers() {
        return observers;
    }
}
