/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import java.util.HashMap;
import java.util.Map;
import model.units.Unit;

/**
 *
 * @author miha
 */
public interface Observable {

    /**
     * Add new observer, that will be done by notifing observers with this event
     *
     * @param event
     * @param observer
     */
    public void registerObserver(String event, Observer observer);

    /**
     * Removing observer from the event
     *
     * @param event
     * @param observer
     */
    public void removeObserver(String event, Observer observer);

    /**
     * Remove event with all observers
     *
     * @param event
     */
    public void removeEvent(String event);

    /**
     * Calling all observers that registered on this event
     *
     * @param event
     * @param params
     */
    public void notifyObservers(String event, Map<String, Object> params);
}
