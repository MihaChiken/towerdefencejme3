/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events;

import java.util.HashMap;
import java.util.Map;
import model.units.Unit;

/**
 *
 * @author miha
 */
public interface Observer {

    /**
     * Making some action that is specified for this observer
     *
     * @param params is a HashMap that consist params from controller
     */
    public void update(Map<String, Object> params);
}
