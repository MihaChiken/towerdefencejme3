/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.controller;

import events.Observer;
import java.util.HashMap;
import java.util.Map;
import model.User;
import model.towers.AbstractBattleTower;
import model.towers.ShootableTower;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 * Perform shooting if there are units in towers radius
 *
 * @author miha
 */
public class TowerHitObserver implements Observer {

    private UnitSequence unitSequence;
    public static final String PARAMS_TOWER = "tower"; 
    public static final String PARAMS_UNIT = "unit";
    

    public TowerHitObserver(UnitSequence unitSequence) {
        this.unitSequence = unitSequence;
    }

    @Override
    public void update(Map<String, Object> params) {

        Object objTower = params.get(PARAMS_TOWER);
        AbstractBattleTower tower = (AbstractBattleTower) objTower;
        
        Unit unit = (Unit)params.get(PARAMS_UNIT);
        tower.targetHit(unit, unitSequence.getUnitsOnMap());
    }
}
