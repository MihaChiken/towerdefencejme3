/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.controller;

import events.Observer;
import java.util.HashMap;
import java.util.Map;
import model.User;
import model.towers.AbstractBattleTower;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 * Perform shooting if there are units in towers radius
 *
 * @author miha
 */
public class TowerReadyObserver implements Observer {

    private UnitSequence unitSequence;
    public static final String PARAMS_TOWER = "tower";
    public static final String PARAMS_UNIT = "units";

    public TowerReadyObserver(UnitSequence unitSequence) {
        this.unitSequence = unitSequence;
    }

    @Override
    public void update(Map<String, Object> params) {

        Object objTower = params.get(PARAMS_TOWER);
        AbstractBattleTower tower = (AbstractBattleTower) objTower;
        UnitGroup group = unitSequence.getUnitsInRadius(tower.getPoint(), tower.getRadius());
        if (group.getUnits().size() > 0) {
            Object objUnit = params.get(PARAMS_UNIT);
            objUnit = tower.shoot(group);
            params.put(PARAMS_UNIT, objUnit);
        }
    }
}
