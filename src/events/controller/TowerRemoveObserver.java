/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.controller;

import events.Observer;
import java.util.Map;
import model.User;
import model.towers.Tower;

/**
 *
 * @author miha
 */
public class TowerRemoveObserver implements Observer{
    public static final String PARAM_TOWER = "tower";

    private User user;

    public TowerRemoveObserver(User user) {
        this.user = user;
    }
    
    
    @Override
    public void update(Map<String, Object> params) {
        if(params.containsKey(PARAM_TOWER)){
            Tower tower = (Tower) params.get(PARAM_TOWER);
            user.setGold(user.getGold() + tower.getCost()/2);            
        }
    }
    
}
