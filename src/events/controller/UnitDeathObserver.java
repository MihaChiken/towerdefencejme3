/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.controller;

import events.Observer;
import java.util.HashMap;
import java.util.Map;
import model.User;
import model.units.Unit;

/**
 * Give reward to the user from dead unit
 *
 * @author miha
 */
public class UnitDeathObserver implements Observer {

    private User user;
    public static final String PARAMS_NAME = "unit";

    public UnitDeathObserver(User user) {
        this.user = user;
    }

    @Override
    public void update(Map<String, Object> params) {
        if (params.containsKey(PARAMS_NAME)) {
            Object obj = params.get(PARAMS_NAME);
            Unit unit = (Unit) obj;
            user.addGold(unit.getReward());
        }
    }
}
