/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.controller;

import events.Observer;
import java.util.HashMap;
import java.util.Map;
import model.User;
import model.units.Unit;

/**
 * Take one life from the user when unit came to the target
 *
 * @author miha
 */
public class UnitFinishedObserver implements Observer {

    private User user;

    public UnitFinishedObserver(User user) {
        this.user = user;
    }

    @Override
    public void update(Map<String, Object> params) {
        user.subOneLife();
    }
}
