/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import events.Observer;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import model.towers.AbstractBattleTower;
import model.towers.Bullet;
import model.towers.ShootableTower;
import model.towers.Tower;
import model.towers.TowersMap;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 *
 * @author miha
 */
public class DisplayBulletsObserver implements Observer {

    private static final Map<Bullet, Spatial> bulletSpatials = new HashMap<Bullet, Spatial>();
    private TowersMap towersMap;
    private Node node;
    private AssetManager assetManager;

    public DisplayBulletsObserver(TowersMap towersMap, Node node, AssetManager assetManager) {
        this.towersMap = towersMap;
        this.node = node;
        this.assetManager = assetManager;
    }

    @Override
    public void update(Map<String, Object> params) {
        removeOldBullets();

        for (AbstractBattleTower tower : towersMap.getBattleTowers()) {
            if (tower.getBullets().size() > 0) {
                for (Bullet bullet : tower.getBullets()) {
                    if (!bulletSpatials.containsKey(bullet)) {
                        node.attachChild(newBullet(bullet, tower));
                    } else {
                        bulletSpatials.get(bullet).setLocalTranslation(new Vector3f(bullet.getBulletPoint().getY(), 1f, bullet.getBulletPoint().getX()));
                    }
                }
            }
        }
    }

    private void removeOldBullets() {
        Iterator<Bullet> i = bulletSpatials.keySet().iterator();
        while (i.hasNext()) {
            Bullet bullet = i.next();
            if (bullet.isHit()) {
                node.detachChild(bulletSpatials.get(bullet));
                i.remove();
            }
        }
    }

    private Spatial newBullet(Bullet bullet, ShootableTower tower) {
        Box b = new Box(
                new Vector3f(bullet.getBulletPoint().getY(), 1f, bullet.getBulletPoint().getX()).normalize(),
                0.2f, 0.2f, 0.2f);
        Geometry box = new Geometry("Box", b);

        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Green);
        if (tower.getBulletModel().equals("frost_bullet")) {
            mat.setColor("Color", ColorRGBA.Blue);
        }
        box.setMaterial(mat);
        bulletSpatials.put(bullet, box);
        return box;

    }
}