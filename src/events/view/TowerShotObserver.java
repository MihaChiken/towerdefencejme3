/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import events.Observer;
import java.util.Map;

/**
 * Display tower shoot animation
 *
 * @author miha
 */
public class TowerShotObserver implements Observer {

    private Map<Integer, Spatial> spatials;
    private Node node;
    public static final String PARAMS_TOWER = "tower";
    public static final String PARAMS_UNIT = "unit";

    public TowerShotObserver(Map<Integer, Spatial> spatials, Node node) {
        this.spatials = spatials;
        this.node = node;
    }

    @Override
    public void update(Map<String, Object> params) {
        System.out.println("shot!");
    }
}
