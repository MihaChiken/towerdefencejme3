/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import events.Observer;
import com.jme3.animation.AnimChannel;
import com.jme3.animation.AnimControl;
import com.jme3.animation.AnimEventListener;
import com.jme3.animation.LoopMode;
import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.util.HashMap;
import java.util.Map;
import model.Point;
import model.units.Unit;

/**
 * Deisplay unit moving
 *
 * @author miha
 */
public class UnitMoveObserver implements Observer, AnimEventListener {

    private Map<Integer, Spatial> spatials;
    private Node node;
    private AssetManager assetManager;
    public static final String PARAMS_UNIT = "unit";
    private AnimChannel channel;
    private AnimControl control;

    public UnitMoveObserver(Map<Integer, Spatial> spatials, Node node, AssetManager assetManager) {
        this.node = node;
        this.spatials = spatials;
        this.assetManager = assetManager;
    }

    @Override
    public void update(Map<String, Object> params) {
        if (params.containsKey(PARAMS_UNIT)) {
            Object obj = params.get(PARAMS_UNIT);
            Unit unit = (Unit) obj;
            int id = unit.hashCode();
            if (spatials.containsKey(id)) {
                Spatial unitsModel = spatials.get(id);

                unitsModel.setLocalTranslation(new Vector3f(unit.getPoint().getY(), 1.5f, unit.getPoint().getX()));
                unitsModel.lookAt(new Vector3f(unit.getNextNodePoint().getY(), 1.5f, unit.getNextNodePoint().getX()), Vector3f.ZERO);

            } else {
                if (unit.getPoint().getX() >= 0) {
                    spatials.put(id, newUnitSpatial(unit));
                }
            }
        }

    }

    private Spatial newUnitSpatial(Unit unit) {

        
        Node unitModel = (Node)assetManager.loadModel("Models/runner2/runner2.j3o");
        unitModel.setLocalTranslation(unit.getPoint().getY(), 1.5f, unit.getPoint().getX());
        unitModel.scale(.5f);
        
        node.attachChild(unitModel);
        control = ((Node)unitModel.getChild("Skeleton")).getChild("Gus").getControl(AnimControl.class);
        control.addListener(this);
        channel = control.createChannel();
        channel.setAnim("SeletonAction", 0.50f);
        channel.setLoopMode(LoopMode.Loop);
        channel.setSpeed(3f);

        return unitModel;
    }

    @Override
    public void onAnimCycleDone(AnimControl control, AnimChannel channel, String animName) {
        
    }

    @Override
    public void onAnimChange(AnimControl control, AnimChannel channel, String animName) {
        
    }
}
