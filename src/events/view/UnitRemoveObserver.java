/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import events.Observer;
import com.jme3.asset.AssetManager;
import com.jme3.font.BitmapText;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Map;
import model.User;
import model.units.Unit;

/**
 * Removes unit from the map
 *
 * @author miha
 */
public class UnitRemoveObserver implements Observer {

    private Map<Integer, Spatial> spatials;
    private Node node;
    public static final String PARAMS_UNIT = "unit";

    public UnitRemoveObserver(Map<Integer, Spatial> spatials, Node node) {
        this.node = node;
        this.spatials = spatials;
    }

    @Override
    public void update(Map<String, Object> params) {
        if (params.containsKey(PARAMS_UNIT)) {
            Object obj = params.get(PARAMS_UNIT);
            Unit unit = (Unit) obj;

            node.detachChild(spatials.get(unit.hashCode()));
            spatials.remove(unit.hashCode());
         
        }
    }
}
