/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import events.Observer;
import com.jme3.asset.AssetManager;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.Map;
import model.units.Unit;

/**
 * Show reward for the dead unit
 *
 * @author miha
 */
public class UnitRewardObserver implements Observer {

    private Map<Integer, Spatial> spatials;
    private Node node;
    public static final String PARAMS_UNIT = "unit";

    public UnitRewardObserver(Map<Integer, Spatial> spatials, Node node) {
        this.node = node;
        this.spatials = spatials;
    }

    @Override
    public void update(Map<String, Object> params) {
        if (params.containsKey(PARAMS_UNIT)) {
            Object obj = params.get(PARAMS_UNIT);
            Unit unit = (Unit) obj;
            
        }
    }
}
