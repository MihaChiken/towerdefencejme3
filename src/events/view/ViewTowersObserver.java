/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package events.view;

import events.Observer;
import com.jme3.asset.AssetManager;
import com.jme3.light.Light;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.util.Map;
import model.towers.Bullet;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 * Display tower on the map
 *
 * @author miha
 */
public class ViewTowersObserver implements Observer {
    
    private Map<Integer, Spatial> spatials;
    private Node node;
    private AssetManager assetManager;
    public static final String PARAMS_TOWER = "tower";
    public static final String PARAMS_UNIT = "unit";
    
    public ViewTowersObserver(Map<Integer, Spatial> spatials, Node node, AssetManager assetManager) {
        this.spatials = spatials;
        this.node = node;
        this.assetManager = assetManager;
    }
    
    @Override
    public void update(Map<String, Object> params) {
        Object objTower = params.get(PARAMS_TOWER);
        Tower tower = (Tower) objTower;
        if (!spatials.containsKey(tower.hashCode())) {
            Spatial towerSpatial = newTowerSpatial(tower);
            spatials.put(tower.hashCode(), towerSpatial);
            node.attachChild(towerSpatial);
        }
    }
    
    private Spatial newTowerSpatial(Tower tower) {
        Box b = new Box(
                new Vector3f(tower.getPoint().getY(), 0, tower.getPoint().getX()),
                (float) 1, (float) 3, (float) 1);
        Geometry box = new Geometry("Box", b);
        
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
        if(tower.getTowerModel().equals("simple_tower")){
            mat.setColor("Color", ColorRGBA.Gray);
        }
        box.setMaterial(mat);
        
        return box;
    }
}
