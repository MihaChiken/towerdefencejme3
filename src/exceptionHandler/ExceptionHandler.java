package exceptionHandler;

import log.Log;

/**
 * Global exception handler
 *
 * @author miha
 */
public class ExceptionHandler {

    public static void catchIt(Exception e) {
        Log.add("\n\n EXCEPTION " + e.getClass().getSimpleName() + "!!! \n\n");
        e.printStackTrace();

    }
}
