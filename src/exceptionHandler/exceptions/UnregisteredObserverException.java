/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package exceptionHandler.exceptions;

/**
 *
 * @author miha
 */
public class UnregisteredObserverException extends RuntimeException {

    private String observerName;

    public UnregisteredObserverException(String observerName) {

        super("Event with name '" + observerName + "' isn`t registered in the storage.");
        this.observerName = observerName;
    }

    public UnregisteredObserverException(String observerName, String msg) {
        super(msg);
        this.observerName = observerName;
    }

    public String getObserverName() {
        return observerName;
    }
}
