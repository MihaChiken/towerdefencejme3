package log;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Elissey
 */
public class Log {

    private static final String LOG_FILE_PATH = "src/log/game-log.txt";
    private static final boolean DEBUG_MODE = true;

    public static boolean isDebugMode() {
        return DEBUG_MODE;
    }
    public static void clear() {
        if (DEBUG_MODE) {
            File logFile = new File(LOG_FILE_PATH);
            FileWriter writeFile;
            try {
                writeFile = new FileWriter(logFile, false);
                Date date = new Date(System.currentTimeMillis());
                writeFile.write("\n Log file cleared at " + date.getDate() + "|" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + "\n\n\n ");
                writeFile.flush();
                writeFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        
    }

    public static void add(String line) {
        if (DEBUG_MODE) {
            File logFile = new File(LOG_FILE_PATH);
            FileWriter writeFile;
            try {
                writeFile = new FileWriter(logFile, true);
                Date date = new Date(System.currentTimeMillis());
                writeFile.write("(" + date.getDate() + "|" + date.getHours() + ":" + date.getMinutes() + ":" + date.getSeconds() + ")   ");
                writeFile.write(line);
                writeFile.write("\n --- \n");
                writeFile.flush();
                writeFile.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
