package model;

/**
 * Store information about a level
 *
 * @author miha
 */
public class Level {

    private int lifes;
    private int startGoldAmount;
    private NodeChain nodeChain = new SimpleUnitsNodeChain();

    public void setStartGoldAmount(int startGoldAmount) {
        this.startGoldAmount = startGoldAmount;
    }

    public int getStartGoldAmount() {
        return startGoldAmount;
    }

    public void setLifes(int lifes) {
        this.lifes = lifes;
    }

    public int getLifes() {
        return lifes;
    }

    public NodeChain getNodeChain() {
        return nodeChain;
    }
}
