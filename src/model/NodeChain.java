package model;

import java.util.ArrayList;

/**
 * Node chain controlling unit path by setting chain(list) of nodes(points) that
 * units must to come through.
 *
 * @author miha
 */
public interface NodeChain {

    /**
     * Return id of current node
     *
     * @param point
     * @return
     */
    public int getNodeId(Point point);

    /**
     * Adding new node
     *
     * @param point
     * @return
     */
    public Point addNode(Point point);

    /**
     * Getting node from the chain that unit must go to from the current point
     *
     * @param point
     * @return
     */
    public Point getNextNode(Point point);
}
