package model;

import java.lang.Math;

/**
 * Simplify working with coords
 *
 * @author miha
 */
public class Point {

    private float x, y;

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public Point(float x, float y) {
        setXY(x, y);
    }

    /**
     * Return distance from the current point to the other point
     *
     * @param point
     * @return
     */
    public float getDistance(Point point) {
        return (float) Math.sqrt(Math.pow(this.x - point.x, 2) + Math.pow(this.y - point.y, 2));
    }

    public void setXY(float x, float y) {
        this.x = x;
        this.y = y;
    }

    public void addToX(float value) {
        x += value;
    }

    public void addToY(float value) {
        y += value;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj != null) {
            if (obj.getClass().getSimpleName().equals("Point")) {
                Point that = (Point) obj;

                return Math.round(this.x) == Math.round(that.x) && Math.round(this.y) == Math.round(that.y);
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return " (" + x + ";" + y + ") ";
    }

    public void moveInDirection(Point target, float speed) {
        float rollX1, rollX2, rollY1, rollY2, hypotenuse1;
        rollX1 = target.getX() - x;
        rollY1 = target.getY() - y;
        hypotenuse1 = (float) Math.sqrt((double) (rollX1 * rollX1 + rollY1 * rollY1));
        if (hypotenuse1 > speed) {
            rollX2 = (rollX1 * speed) / hypotenuse1;
            rollY2 = (rollY1 * speed) / hypotenuse1;
        } else {
            rollX2 = rollX1;
            rollY2 = rollY1;
        }

        addToX(rollX2);
        addToY(rollY2);
    }
}
