package model;

import java.util.ArrayList;
import java.util.List;

/**
 * Allows creating a simple path with only one branch
 *
 * @author miha
 */
public class SimpleUnitsNodeChain implements NodeChain {

    private List<Point> chain = new ArrayList<Point>();

    public SimpleUnitsNodeChain() {
    }

    @Override
    public int getNodeId(Point point) {
        return chain.indexOf(point);
    }

    @Override
    public Point addNode(Point point) {
        chain.add(point);
        return point;
    }

    @Override
    public Point getNextNode(Point point) {
        if (chain.contains(point)) {
            int i = chain.indexOf(point);
            i++;
            if (i >= chain.size()) {
                return null;
            }
            return chain.get(i);
        } else {
            return chain.get(0);
        }

    }
}
