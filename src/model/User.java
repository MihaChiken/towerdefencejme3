/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 * Store user information
 *
 * @author miha
 */
public class User {

    private String name;
    private int gold;
    private int lifes;

    public User(String name, int gold, int lifes) {
        this.name = name;
        this.gold = gold;
        this.lifes = lifes;
    }

    public void setLifes(int lifes) {
        this.lifes = lifes;
    }

    public int getLifes() {
        return lifes;
    }

    public void subOneLife() {
        lifes--;
    }

    public void setGold(int gold) {
        this.gold = gold;
    }

    public void addGold(int gold) {
        this.gold += gold;
    }

    public int getGold() {
        return gold;
    }

    @Override
    public String toString() {
        return "Gold:"+gold+" | Lifes:"+lifes;
    }
    
    
}
