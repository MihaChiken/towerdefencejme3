/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import com.jme3.scene.Spatial;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import model.Point;
import model.towers.buffs.TowersBuff;
import model.units.Unit;
import model.units.UnitGroup;

/**
 *
 * @author miha
 */
public abstract class AbstractBattleTower implements Tower, ShootableTower {

    private List<TowersBuff> buffs = new ArrayList<TowersBuff>();
    private List<Bullet> bullets = new ArrayList<Bullet>();
    private String towerModel;
    private String bulletModel;
    private final int defaultCooldown;
    private final float defaultRadius;
    private final int defaultDamage;
    private Point point;
    private int cost;
    private int currentCooldown;
    private int totalCooldownTime;
    private float radius;
    private int damage;

    @Override
    public String getBulletModel() {
        return bulletModel;
    }

    @Override
    public String getTowerModel() {
        return towerModel;
    }
    private static final float bulletSpeed = 1.4f;

    @Override
    public float getRadius() {
        return radius;
    }

    public int getTotalCooldownTime() {
        return totalCooldownTime;
    }

    @Override
    public void attachBuff(TowersBuff buff) {
        if (buffs.contains(buff)) {
            buffs.remove(buff);
        }
        buffs.add(buff);
    }

    public void setCooldown(int cooldown) {
        this.currentCooldown = cooldown;
    }

    public AbstractBattleTower(Point point, float radius, int cooldown, int cost, int damage, String towerModel, String bulletModel) {
        this.point = point;
        this.radius = defaultRadius = radius;
        this.currentCooldown = 0;
        defaultCooldown = totalCooldownTime = cooldown;
        this.cost = cost;
        this.damage = defaultDamage = damage;
        this.towerModel = towerModel;
        this.bulletModel = bulletModel;
    }

    public int getDamage() {
        return damage;
    }

    @Override
    public boolean isTowerReady() {
        return currentCooldown == 0;
    }

    @Override
    public int getCooldown() {
        return currentCooldown;
    }

    @Override
    public int getCost() {
        return cost;
    }

    @Override
    public Point getPoint() {
        return point;
    }

    @Override
    public float getDistance(Point point) {
        return point.getDistance(point);
    }

    @Override
    public boolean checkDistance(Point point) {
        return point.getDistance(point) <= radius;
    }

    @Override
    public UnitGroup shoot(UnitGroup units) {
        Unit unit = units.getNearestUnitTo(point);
        Bullet bullet = new Bullet(new Point(point.getX(), point.getY()), unit, bulletModel);
        bullets.add(bullet);
        currentCooldown = totalCooldownTime;
        return new UnitGroup(false, unit);
    }

    @Override
    public boolean runTowerLoop() {
        decrementCooldown();
        checkBuffs();
        setupBuffs();
        return true;
    }

    @Override
    public String toString() {
        return "Tower (" + this.getPoint().getX() + "|" + this.getPoint().getY() + "): " + this.getCooldown();
    }

    private void decrementCooldown() {
        if (currentCooldown > 0) {
            currentCooldown--;
        }
    }

    private void setupBuffs() {
        damage = defaultDamage;
        totalCooldownTime = defaultCooldown;
        radius = defaultRadius;

        for (TowersBuff buff : buffs) {
            damage = buff.setDamage(damage);
            totalCooldownTime = buff.setCooldown(totalCooldownTime);
            radius = buff.setRadius(radius);
        }
    }

    private void checkBuffs() {
        Iterator<TowersBuff> i = buffs.iterator();
        while (i.hasNext()) {
            TowersBuff buff = i.next();
            if (buff.decrementTime() == 0) {
                i.remove();
            }
        }
    }

    @Override
    public Unit getHittedUnit() {
        Unit hittedUnit = null;
        if (!bullets.isEmpty()) {
            Iterator<Bullet> i = bullets.iterator();
            while (i.hasNext()) {
                Bullet bullet = i.next();
                Unit unit = bullet.getHittedTarget();
                if (unit == null) {
                    bullet.getBulletPoint().moveInDirection(bullet.getBulletTarget(), bulletSpeed);
                } else {
                    i.remove();
                    hittedUnit = unit;
                }
            }
        }
        return hittedUnit;
    }

    @Override
    public List<Bullet> getBullets() {
        return bullets;
    }
}
