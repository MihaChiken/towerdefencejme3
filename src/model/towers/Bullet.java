/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import java.security.Timestamp;
import model.Point;
import model.units.Unit;

/**
 *
 * @author miha
 */
public class Bullet {

    private Point bulletPoint;
    private Point bulletTarget;
    private Unit target;
    private String modelName;
    private final Long shootingTime = new Long(System.currentTimeMillis());
    private final Point startPoint;
    private boolean isHit = false;

    public boolean isHit() {
        return isHit;
    }

    public Bullet(Point bulletPoint, Unit target, String modelName) {
        this.bulletPoint = startPoint = bulletPoint;
        this.bulletTarget = target.getPoint();
        this.target = target;
        this.modelName = modelName;
    }

    public String getModelName() {
        return modelName;
    }

    public Point getBulletPoint() {
        return bulletPoint;
    }

    public void setBulletTarget(Point bulletTarget) {
        this.bulletTarget = bulletTarget;
    }

    public Point getBulletTarget() {
        return bulletTarget;
    }

    public void setBulletPoint(Point bulletPoint) {
        this.bulletPoint = bulletPoint;
    }

    public Unit getHittedTarget() {
        if (bulletTarget.equals(bulletPoint)) {
            isHit = true;
            return target;
        } else {
            return null;
        }
    }

    public Point getStartPoint() {
        return startPoint;
    }

    public Long getShootingTime() {
        return shootingTime;
    }

    @Override
    public int hashCode() {
        return shootingTime.hashCode() + modelName.hashCode() + startPoint.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bullet other = (Bullet) obj;
        if ((this.modelName == null) ? (other.modelName != null) : !this.modelName.equals(other.modelName)) {
            return false;
        }
        if (this.shootingTime != other.shootingTime) {
            return false;
        }
        if (this.startPoint != other.startPoint && (this.startPoint == null || !this.startPoint.equals(other.startPoint))) {
            return false;
        }
        return true;
    }
}
