/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.Point;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.buffs.AbstractUnitsTimeBuff;
import model.units.buffs.UnitsBuff;

/**
 *
 * @author miha
 */
public class FrostTower extends AbstractBattleTower {

    private static final float radius = 15;
    private static final int damage = 120;
    private static final int cooldawn = 60;
    private static final int cost = 70;
    private static final String towerModel = "frost_tower";
    private static final String bulletModel = "frost_bullet";

    public FrostTower(Point point) {
        super(point, radius, cooldawn, cost, damage, towerModel, bulletModel);
    }

    @Override
    public UnitGroup targetHit(Unit target, UnitGroup units) {
        target.makeDamage(super.getDamage());
        target.attachBuff(new FrostBuff());

        return new UnitGroup(false, target);
    }

    class FrostBuff extends AbstractUnitsTimeBuff {

        public FrostBuff() {
            super(true, 100);
        }

        @Override
        public int setHealth(int health) {
            return health;
        }

        @Override
        public float setSpeed(float speed) {
            return speed / 3;
        }
    }
}
