/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import java.util.List;
import model.towers.buffs.TowersBuff;
import model.units.Unit;
import model.units.UnitGroup;

/**
 *
 * @author miha
 */
public interface ShootableTower {
    /**
     * Return towers cooldown.
     *
     * @return
     */
    public boolean isTowerReady();

    public int getCooldown();

    public void attachBuff(TowersBuff buff);

    /**
     * Making specific activity for each tower
     *
     * @param units
     * @return unit that takes damage
     */
    public UnitGroup shoot(UnitGroup units);

    public UnitGroup targetHit(Unit target,UnitGroup units);
    
    public Unit getHittedUnit();
    
    public List<Bullet> getBullets();
    
    public String getBulletModel();
    
}
