/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import model.Point;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.buffs.AbstractUnitsTimeBuff;
import model.units.buffs.UnitsBuff;

/**
 *
 * @author miha
 */
public class SimpleTower extends AbstractBattleTower {

    private static final float radius = 20;
    private static final int damage = 100;
    private static final int cooldawn = 40;
    private static final int cost = 60;
    private static final String towerModel = "simple_tower";
    private static final String bulletModel = "simple_bullet";

    public SimpleTower(Point point) {
        super(point, radius, cooldawn, cost, damage, towerModel, bulletModel);
    }

    @Override
    public UnitGroup targetHit(Unit target, UnitGroup units) {
        target.makeDamage(super.getDamage());

        return new UnitGroup(false, target);
    }
}
