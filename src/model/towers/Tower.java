/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import com.jme3.scene.Spatial;
import java.util.List;
import java.util.Map;
import model.Point;
import model.towers.buffs.TowersBuff;
import model.units.Unit;
import model.units.UnitGroup;

/**
 *
 * @author miha
 */
public interface Tower {

    /**
     *
     * @return cost of tower building
     */
    public int getCost();

    public Point getPoint();

    public float getRadius();

    public float getDistance(Point point);

    /**
     * Check distance between tower and some point.
     *
     * @param point
     * @return true if this distance is lower that tower radius
     */
    public boolean checkDistance(Point point);

    public boolean runTowerLoop();
    
    public String getTowerModel();
}
