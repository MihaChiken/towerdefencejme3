/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;
import model.Point;

/**
 * Store all the towers on the map
 *
 * @author miha
 */
public class TowersMap {

    private static List<Tower> towers = new ArrayList<Tower>();

    public List<Tower> getTowers() {
        return towers;
    }
    public List<AbstractBattleTower> getBattleTowers() {
        List<AbstractBattleTower> battleTowers = new ArrayList<AbstractBattleTower>();
        for(Tower tower: towers){
            if(tower instanceof AbstractBattleTower){
                battleTowers.add((AbstractBattleTower)tower);
            }
        }
        return battleTowers;
    }
    public boolean isFreeSpace(Point point){
        for(Tower tower:towers){
            if(tower.getPoint().equals(point)){
                return false;
            }
        }
        return true;
    }
}
