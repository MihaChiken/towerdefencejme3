/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers.buffs;

/**
 *
 * @author miha
 */
public abstract class AbstractTowerTimeBuff implements TowersBuff {
    private boolean debuff;
    private int currentTime;

    public AbstractTowerTimeBuff(boolean debuff, int time) {
        this.debuff = debuff;
        this.currentTime=time;
    }
    
    @Override
    public abstract int setDamage(int damage);

    @Override
    public abstract int setCooldown(int cooldown);

    @Override
    public abstract float setRadius(Float radius);

    @Override
    public int decrementTime() {
        return currentTime--;
    }
    @Override
    public int getTime() {
        return currentTime;
    }
    
    
    @Override
    public boolean isDebuff() {
        return debuff;
    }
}
