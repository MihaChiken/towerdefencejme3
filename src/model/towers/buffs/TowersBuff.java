/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.towers.buffs;

/**
 *
 * @author miha
 */
public interface TowersBuff {
    
    public int setDamage(int damage);
    
    public int setCooldown(int cooldown);
    
    public float setRadius(Float radius);

    public int decrementTime();
    
    public int getTime();

    public boolean isDebuff();
}
