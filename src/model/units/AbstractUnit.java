/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import model.NodeChain;
import model.Point;
import model.towers.buffs.TowersBuff;
import model.units.buffs.AbstractUnitsTimeBuff;
import model.units.buffs.UnitsBuff;

/**
 *
 * @author miha
 */
public class AbstractUnit implements Unit, Cloneable {

    private final float defaultSpeed;
    private List<UnitsBuff> buffs = new ArrayList<UnitsBuff>();
    private int level;
    private final Point startPoint;
    private int reward;
    private Point point;
    private Point pathNode;
    private int health;
    private float speed;

    @Override
    public Point getNextNodePoint() {
        return pathNode;
    }

    public AbstractUnit(Point point, int health, int reward, float speed, int level) {
        startPoint = new Point(point.getX(), point.getY());
        this.point = point;
        this.health = health;
        this.speed = defaultSpeed = speed;
        this.reward = reward;
        this.level = level;
    }

    @Override
    public Point getStartPoint() {
        return startPoint;
    }

    @Override
    public int getHealth() {
        return health;
    }

    @Override
    public Point getPoint() {
        return point;
    }

    @Override
    public void makeDamage(int damage) {
        health -= damage;
    }

    @Override
    public int move(NodeChain nodeChain) {
        setupBuffs();

        if (pathNode == null) {
            pathNode = nodeChain.getNextNode(point);
        } else {
            if (point.equals(pathNode)) {
                pathNode = nodeChain.getNextNode(point);
            }
        }
        if (pathNode == null) {
            return FINISHED;
        }

        point.moveInDirection(pathNode, speed);
        if (health > 0) {
            return ALIVE;
        } else {
            return DEAD;
        }
    }

    @Override
    public float getSpeed() {
        return speed;
    }

    @Override
    public void attachBuff(UnitsBuff buff) {
        if(buffs.contains(buff)){
            buffs.remove(buff);
        }
        buffs.add(buff);
    }

    @Override
    public int getReward() {
        return reward;
    }

    @Override
    public String toString() {
        return "Unit " + this.hashCode() + "(" + this.getPoint().getX() + "|" + this.getPoint().getY() + ") : " + this.getHealth();
    }

    private void setupBuffs() {
        speed = defaultSpeed;

        Iterator<UnitsBuff> i = buffs.iterator();
        while (i.hasNext()) {
            UnitsBuff buff = i.next();
            if (buff.decrementTime() != 0) {
                speed = buff.setSpeed(speed);
                health = buff.setHealth(health);
            } else {
                i.remove();
            }
        }
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return new AbstractUnit(point, health, reward, speed, level);
    }
}
