/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units;

import model.Point;

/**
 *
 * @author miha
 */
public class SimpleUnit extends AbstractUnit {

    private static final int health = 400;
    private static final int reward = 8;
    private static final float speed = 0.2f;

    public SimpleUnit(Point point) {
        super(point, health, reward, speed, 0);
    }

    public SimpleUnit(Point point, int level) {
        super(point, health + 70 * level, reward + 3 * level, speed + 0.02f * level, level);

    }
}
