/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units;

import model.NodeChain;
import model.Point;
import model.units.buffs.UnitsBuff;

/**
 *
 * @author miha
 */
public interface Unit {

    public int ALIVE = 0;
    public int DEAD = 1;
    public int FINISHED = 2;

    public float getSpeed();

    public int getHealth();

    public int getReward();

    public Point getPoint();
    
    public Point getNextNodePoint();

    public Point getStartPoint();
    
    public void attachBuff(UnitsBuff buff);

    /**
     * Substract damage from units health
     *
     * @param damage
     */
    public void makeDamage(int damage);

    /**
     * Move unit in direction to get next node in the chain
     *
     * @param nodeChain
     * @return 0 - if unit is alive, 1 - if unit is dead, 2 - if unit came to
     * the target
     */
    public int move(NodeChain nodeChain);
}
