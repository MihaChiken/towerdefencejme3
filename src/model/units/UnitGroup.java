/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import model.Point;

/**
 * Store group of the unit
 *
 * @author miha
 */
public class UnitGroup {

    private boolean onMap;
    private List<Unit> units;

    public UnitGroup(boolean onMap, List<Unit> units) {
        this.onMap = onMap;
        this.units = units;
    }
    public UnitGroup(boolean onMap, Unit ... unitsArray){
        this.onMap = onMap;
        units = new ArrayList<Unit>();
        units.addAll(Arrays.asList(unitsArray));
    }

    public boolean isEmpty() {
        return units.isEmpty();
    }

    public boolean isOnMap() {
        return onMap;
    }

    public void setOnMap(boolean onMap) {
        this.onMap = onMap;
    }

    public Unit addUnit(Unit unit) {
        units.add(unit);
        return unit;
    }

    /**
     * Return from the group a unit that are nearest to the some point
     *
     * @param point
     * @return
     */
    public Unit getNearestUnitTo(Point point) {
        Iterator<Unit> unitIt = units.iterator();
        Unit nearestUnit = unitIt.next();
        float nearestDistance = point.getDistance(nearestUnit.getPoint());
        float distance;

        while (unitIt.hasNext()) {
            Unit unit = unitIt.next();
            distance = point.getDistance(unit.getPoint());
            if (distance < nearestDistance) {
                nearestDistance = distance;
                nearestUnit = unit;
            }
        }
        return nearestUnit;
    }

    public List<Unit> getUnits() {
        return units;
    }
}
