/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units;

import java.util.ArrayList;
import java.util.List;
import model.Point;

/**
 * Store unit groups sequence
 *
 * @author miha
 */
public class UnitSequence {

    private static List<UnitGroup> unitGroups = new ArrayList<UnitGroup>();

    /**
     * Set next group on the map
     *
     * @return null if there are no groups in a sequence
     */
    public UnitGroup nextGroup() {
        for (UnitGroup unitGroup : unitGroups) {
            if (!unitGroup.isOnMap() && !unitGroup.isEmpty()) {
                unitGroup.setOnMap(true);
                return unitGroup;
            }
        }
        return null;
    }

    public List<UnitGroup> getUnitsGroups() {
        return unitGroups;
    }

    /**
     * @return All groups on the map
     */
    public List<UnitGroup> getUnitsGroupsOnMap() {
        List<UnitGroup> groups = new ArrayList<UnitGroup>();
        for (UnitGroup group : unitGroups) {
            if (group.isOnMap()) {
                groups.add(group);
            }
        }
        return groups;
    }

    /**
     * @param point
     * @param radius
     * @return all units that locate in the some radius arround the point
     */
    public UnitGroup getUnitsInRadius(Point point, float radius) {
        List<Unit> group = new ArrayList<Unit>();
        for (UnitGroup unitGroup : getUnitsGroupsOnMap()) {
            for (Unit unit : unitGroup.getUnits()) {
                if (unit.getPoint().getDistance(point) <= radius) {
                    group.add(unit);
                }
            }
        }
        return new UnitGroup(false, group);
    }

    public UnitGroup getUnitsOnMap() {
        List<Unit> group = new ArrayList<Unit>();
        for (UnitGroup unitGroup : getUnitsGroupsOnMap()) {
            for (Unit unit : unitGroup.getUnits()) {
                group.add(unit);
            }
        }
        return new UnitGroup(false, group);
    }
}
