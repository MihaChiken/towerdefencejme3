/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units.buffs;

/**
 *
 * @author miha
 */
public abstract class AbstractUnitsTimeBuff implements UnitsBuff{
    private boolean debuff;
    private int currentTime;

    public AbstractUnitsTimeBuff(boolean debuff, int time) {
        this.debuff = debuff;
        this.currentTime=time;
    }
    
    @Override
    public abstract int setHealth(int health);

    @Override
    public abstract float setSpeed(float speed);


    @Override
    public int decrementTime() {
        return currentTime--;
    }

    @Override
    public boolean isDebuff() {
        return debuff;
    }

    @Override
    public boolean equals(Object obj) {
        return this.getClass().getName().equals(obj.getClass().getName());
    }
    
    
    
}
