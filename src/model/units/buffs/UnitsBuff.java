/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.units.buffs;

/**
 *
 * @author miha
 */
public interface UnitsBuff {

    public int setHealth(int health);

    public float setSpeed(float speed);

    public int decrementTime();

    public boolean isDebuff();
}
