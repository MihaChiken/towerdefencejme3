/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import model.User;

/**
 *
 * @author miha
 */
public class HUDController implements ScreenController {

    private Main main;
    private Nifty nifty;
    private User user;
    private Screen screen;

    public HUDController(Main main, Nifty nifty, User user) {
        this.main = main;
        this.nifty = nifty;
        this.user = user;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
    }

    @Override
    public void onStartScreen() {
    }

    @Override
    public void onEndScreen() {
    }

    public void changeTowerType(String type){
        main.setTowerType(type);    
    }
}
