package view;

import events.EventStorage;
import events.view.TowerShotObserver;
import events.view.UnitMoveObserver;
import events.view.UnitRemoveObserver;
import events.view.ViewTowersObserver;
import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.*;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Matrix3f;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.niftygui.NiftyJmeDisplay;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import com.jme3.terrain.geomipmap.TerrainGrid;
import com.jme3.texture.Texture;
import controller.GameController;
import controller.initializer.SampleInit;
import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.builder.ScreenBuilder;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;
import de.lessvoid.nifty.tools.TimeProvider;
import events.view.DisplayBulletsObserver;
import java.lang.reflect.Constructor;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import log.Log;
import model.NodeChain;
import model.Point;
import model.towers.SimpleTower;
import model.towers.Tower;
import model.towers.TowersMap;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 * test
 *
 * @author normenhansen
 */
public class Main extends SimpleApplication {
   
    private Nifty nifty;
    private float cameraSpeed = 70;
    private Map<Integer, Spatial> unitSpatials = new HashMap<Integer, Spatial>();
    private Map<Integer, Spatial> towerSpatials = new HashMap<Integer, Spatial>();
    private Spatial sceneModel;
    private static Node towersNode, unitsNode, textNode;
    private Boolean isRunning = false;
    private GameController controller;
    private String towerType = "SimpleTower";

    public void setTowerType(String towerType) {
        this.towerType = towerType;
    }

    public static void main(String[] args) {
        Main app = new Main();
        Log.clear();
        app.setPauseOnLostFocus(false);
        app.start();
    }

    @Override
    public void simpleInitApp() {

        NiftyJmeDisplay niftyDisplay = new NiftyJmeDisplay(
                assetManager, inputManager, audioRenderer, guiViewPort);
        nifty = niftyDisplay.getNifty();
        nifty.fromXml("Interface/StartMenu.xml", "start", new StartMenuController(this, nifty));
      
        guiViewPort.addProcessor(niftyDisplay);
        // disable the fly cam
        flyCam.setDragToRotate(true);
        controller = GameController.init(new SampleInit());

        towersNode = new Node();
        unitsNode = new Node();
        textNode = new Node();

        rootNode.attachChild(towersNode);
        rootNode.attachChild(unitsNode);
        rootNode.attachChild(textNode);

        sceneModel = assetManager.loadModel("Scenes/SimpleLevel.j3o");
        sceneModel.move(80, 0, 80);

        DirectionalLight sun = new DirectionalLight();
        sun.setDirection(new Vector3f(70, 50, -70).normalizeLocal());
        sun.setColor(ColorRGBA.White);

        rootNode.addLight(sun);

        rootNode.attachChild(sceneModel);

        flyCam.setEnabled(false);

        cam.lookAt(new Vector3f(30, -23, 10), Vector3f.UNIT_Y);
        cam.setLocation(new Vector3f(-30, 50, 60));
        cam.update();

        inputManager.setCursorVisible(false);
        initKeys();


        controller.registerObserver(EventStorage.UNIT_MOVE, new UnitMoveObserver(unitSpatials, unitsNode, assetManager));
        controller.registerObserver(EventStorage.UNIT_DEATH, new UnitRemoveObserver(unitSpatials, unitsNode));
        controller.registerObserver(EventStorage.UNIT_FINISHED, new UnitRemoveObserver(unitSpatials, unitsNode));

        controller.registerObserver(EventStorage.TOWER_READY, new ViewTowersObserver(towerSpatials, towersNode, assetManager));
        controller.registerObserver(EventStorage.GAME_CONTROLLER_RUN, new DisplayBulletsObserver(controller.getTowersController().getTowersMap(), towersNode, assetManager));
        controller.registerObserver(EventStorage.TOWER_SHOT, new TowerShotObserver(towerSpatials, towersNode));

        drawPath(controller.getLevel().getNodeChain());

        drawUserInterface();
    }

    /**
     * Custom Keybinding: Map named actions to inputs.
     */
    private void initKeys() {
        inputManager.addMapping("AddTower", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));

        inputManager.addMapping("MoveMouse", new MouseAxisTrigger(MouseInput.AXIS_X, false),
                new MouseAxisTrigger(MouseInput.AXIS_X, true),
                new MouseAxisTrigger(MouseInput.AXIS_Y, false),
                new MouseAxisTrigger(MouseInput.AXIS_Y, true));
        // You can map one or several inputs to one named action
        inputManager.addMapping("Pause", new KeyTrigger(KeyInput.KEY_P));

        inputManager.addMapping("MoveCameraLeft", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("MoveCameraRight", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("MoveCameraForward", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("MoveCameraBack", new KeyTrigger(KeyInput.KEY_S));
        // Add the names to the action listener.
        inputManager.addListener(actionListener, new String[]{"Pause"});
        inputManager.addListener(analogListener, new String[]{"MoveCameraLeft", "MoveCameraRight", "MoveCameraForward", "MoveCameraBack"});
        inputManager.addListener(analogListener, new String[]{"AddTower", "MoveMouse"});
    }
    private ActionListener actionListener = new ActionListener() {
        @Override
        public void onAction(String name, boolean keyPressed, float tpf) {
            if (name.equals("Pause") && !keyPressed) {
                setIsRunning((Boolean) !getIsRunning());
            }
        }
    };
    private AnalogListener analogListener = new AnalogListener() {
        @Override
        public void onAnalog(String name, float value, float tpf) {
            if (getIsRunning()) {
                if (name.equals("MoveCameraRight")) {
                    Vector3f camLocation = cam.getLocation();
                    if (camLocation.z < 160) {
                        cam.setLocation(new Vector3f(camLocation.x, camLocation.y, camLocation.z + value * speed * cameraSpeed));
                    }

                }
                if (name.equals("MoveCameraLeft")) {
                    Vector3f camLocation = cam.getLocation();
                    if (camLocation.z > 0) {
                        cam.setLocation(new Vector3f(camLocation.x, camLocation.y, camLocation.z - value * speed * cameraSpeed));
                    }
                }
                if (name.equals("MoveCameraBack")) {
                    Vector3f camLocation = cam.getLocation();
                    if (camLocation.x > 0) {
                        cam.setLocation(new Vector3f(camLocation.x - value * speed * cameraSpeed, camLocation.y, camLocation.z));
                    }
                }
                if (name.equals("MoveCameraForward")) {
                    Vector3f camLocation = cam.getLocation();
                    if (camLocation.x < 140) {
                        cam.setLocation(new Vector3f(camLocation.x + value * speed * cameraSpeed, camLocation.y, camLocation.z));
                    }
                }

                if (name.equals("AddTower")) {
                    Vector2f screenPos = inputManager.getCursorPosition();
                    Vector3f click3d = cam.getWorldCoordinates(
                            new Vector2f(screenPos.x, screenPos.y), 0).clone();
                    Vector3f dir = cam.getWorldCoordinates(
                            new Vector2f(screenPos.x, screenPos.y), 1f).subtractLocal(click3d).normalizeLocal();
                    Ray ray = new Ray(click3d, dir);
                    CollisionResults results = new CollisionResults();
                    rootNode.collideWith(ray, results);
                    if (results.size() > 0) {
                        Vector3f r = results.getClosestCollision().getContactPoint();
                        try {
                            Class c = Class.forName("model.towers."+towerType);
                            Class[] paramTypes = new Class[] { Point.class };
                            Constructor aConstrct = c.getConstructor(paramTypes);
                            Tower tower = (Tower) aConstrct.newInstance(new Point(Math.round(r.z), Math.round(r.x)));
                            //Tower tower = new SimpleTower(new Point(Math.round(r.z), Math.round(r.x)));
                            if (controller.getTowersController().checkTowerBuilding(tower, controller.getUser().getGold())) {
                                controller.getTowersController().addTower(tower);
                            }
                        } catch (Exception ex) {
                            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                        }
                        
                    }

                }
            } else {
                System.out.println("Press P to unpause.");
            }
        }
    };

    @Override
    public void simpleUpdate(float tpf) {
        if (isRunning) {
            int status = controller.run();
            BitmapText hudText = (BitmapText) guiNode.getChild("hudText");
            hudText.setText(controller.getUser().toString());

            if (status == GameController.STATUS_PLAYER_LOOSE) {
                hudText.setText("You loose!");
                hudText.setColor(ColorRGBA.Red);
                isRunning = false;
            }
            if (status == GameController.STATUS_PLAYER_WIN) {
                hudText.setText("You win!!");
                hudText.setColor(ColorRGBA.Red);
                isRunning = false;
            }
        }
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }

    /**
     * @return the isRunning
     */
    public Boolean getIsRunning() {
        return isRunning;
    }

    /**
     * @param isRunning the isRunning to set
     */
    public void setIsRunning(Boolean isRunning) {
        this.isRunning = isRunning;
    }

    private void drawPath(NodeChain nodeChain) {
        Point currentNode = null, previousNode = null;
        previousNode = nodeChain.getNextNode(currentNode);
        currentNode = nodeChain.getNextNode(previousNode);
        do {
            float currentY = currentNode.getY(), currentX = currentNode.getX();
            float previousY = previousNode.getY(), previousX = previousNode.getX();

            Box b = new Box(new Vector3f(previousY, 0.15f, previousX), (previousY - currentY) / 2 + 1, 0.1f, (previousX - currentX) / 2 + 1);
            Geometry box = new Geometry("Box", b);
            box.move(-(previousY - currentY) / 2, 0, -(previousX - currentX) / 2);

            Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            Texture texture = assetManager.loadTexture("Textures/stones.jpg");
            b.scaleTextureCoordinates(new Vector2f(1f, 1f));
            mat.setTexture("ColorMap", texture);
            box.setMaterial(mat);

            rootNode.attachChild(box);
            previousNode = currentNode;
            currentNode = nodeChain.getNextNode(currentNode);
        } while (currentNode != null);

    }

    private void drawUserInterface() {
        BitmapText hudText = new BitmapText(guiFont, false);
        hudText.setSize(guiFont.getCharSet().getRenderedSize());      // font size
        hudText.setColor(ColorRGBA.Blue);                             // font color
        hudText.setText(controller.getUser().toString());             // the text
        hudText.setLocalTranslation(300, hudText.getLineHeight()+25, 0); // position
        hudText.setName("hudText");
        guiNode.attachChild(hudText);

    }
}