/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import de.lessvoid.nifty.Nifty;
import de.lessvoid.nifty.screen.Screen;
import de.lessvoid.nifty.screen.ScreenController;

/**
 *
 * @author miha
 */
public class StartMenuController implements ScreenController {

    private Main main;
    private Nifty nifty;
    private Screen screen;

    public StartMenuController(Main main, Nifty nifty) {
        this.main = main;
        this.nifty = nifty;
    }

    @Override
    public void bind(Nifty nifty, Screen screen) {
        this.nifty = nifty;
        this.screen = screen;
    }

    @Override
    public void onStartScreen() {
        
    }

    @Override
    public void onEndScreen() {
    }
    
    public void play(String screen){
        nifty.gotoScreen(screen);
        main.setIsRunning(true);
    }
    public void exit(){
        main.stop();
    }
    
    public void changeTowerType(String type){
        main.setTowerType(type);    
    }
}
