/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.buffs.TestTowerBuff;
import controller.buffs.TestUnitBuff;
import controller.inits.TestPlayerLooseInit;
import events.EventStorage;
import java.util.List;
import model.Point;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import events.controller.TowerReadyObserver;
import model.towers.AbstractBattleTower;
import org.hamcrest.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author miha
 */
public class BuffsTest {
// TODO: several buff test (buff stuck)
    private GameController controller;
    private List<UnitGroup> unitGroups;
    private List<Unit> units;
    private List<Tower> towers;
    private Unit unit;
    private AbstractBattleTower tower;

    public BuffsTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        controller = GameController.initTestMode(new TestPlayerLooseInit());

        units = controller.getUnitsController().getUnitSequence().getUnitsGroups().get(0).getUnits();
        unitGroups = controller.getUnitsController().getUnitSequence().getUnitsGroups();
        towers = controller.getTowersController().getTowersMap().getTowers();

        unit = units.get(0);
        tower = (AbstractBattleTower) towers.get(0);
    }

    @After
    public void tearDown() {
        controller.removeEvent(EventStorage.TOWER_READY);
    }

    @Test
    public void towersBuffsTest() {
        tower.attachBuff(new TestTowerBuff());
        int startUnitsHealth = unit.getHealth();
        controller.run();
        controller.run();
        controller.run();
        assertEquals(tower.getRadius(), 32, 0.0001);
        assertEquals(unit.getHealth(), startUnitsHealth - 11);
        assertEquals(tower.getCooldown(), 7);
        controller.run();
        assertEquals(tower.getRadius(), 32, 0.0001);
        assertEquals(tower.getCooldown(), 6);
        
        for (int i = 0; i < 12; i++) {
            controller.run();
        }
        assertEquals(tower.getRadius(), 32, 0.0001);
        assertEquals(unit.getHealth(), startUnitsHealth - 22);
        assertEquals(tower.getCooldown(), 3);
        
        for (int i = 0; i < 14; i++) {
            controller.run();
        }
        assertEquals(tower.getRadius(), 30, 0.0001);
        assertEquals(unit.getHealth(), startUnitsHealth - 32);
        assertEquals(tower.getCooldown(), 8);
        
    }
    
    @Test
    public void unitsBuffsTest() {
        
        int startUnitsHealth = unit.getHealth();
        
        unit.attachBuff(new TestUnitBuff());
        controller.run();
        
        assertEquals(unit.getHealth(), startUnitsHealth - 2);
        assertEquals(unit.getSpeed(), 0.1f, 0.0001);
        
        controller.run();
        
        assertEquals(unit.getHealth(), startUnitsHealth - 4);
        assertEquals(unit.getSpeed(), 0.1f, 0.0001);
        
        for (int i = 0; i < 9; i++) {
            controller.run();
        }
        
        assertEquals(unit.getSpeed(), 0.2f, 0.0001);
    }
}
