/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.inits.TestPlayerWinInit;
import events.EventStorage;
import java.util.List;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import events.controller.TowerReadyObserver;
import model.Point;
import model.towers.AbstractBattleTower;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.hamcrest.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author miha
 */
public class ControllerPlayerWinTest {

    private GameController controller;
    private List<UnitGroup> unitGroups;
    private List<Unit> units;
    private List<Tower> towers;
    private Unit unit;
    private AbstractBattleTower tower;

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        controller = GameController.initTestMode(new TestPlayerWinInit());

        units = controller.getUnitsController().getUnitSequence().getUnitsGroups().get(0).getUnits();
        unitGroups = controller.getUnitsController().getUnitSequence().getUnitsGroups();
        towers = controller.getTowersController().getTowersMap().getTowers();

        unit = units.get(0);
        tower = (AbstractBattleTower) towers.get(0);
    }

    @After
    public void tearDown() {
        controller.removeEvent(EventStorage.TOWER_READY);
        tower.getBullets().clear();
    }

    @Test
    public void unitDieTest() {
        assertEquals(unitGroups.size(), 2);
        assertEquals(unit.getHealth(), 60);
        controller.run();
        controller.run();
        controller.run();
        assertEquals(unit.getHealth(), 50);
        for (int i = 0; i < 7; i++) {
            controller.run();
        }
        assertEquals(unit.getHealth(), 0);

        assertEquals(unitGroups.size(), 1);
        controller.run();
        assertEquals(controller.run(), GameController.STATUS_NOTHING_HAPPENS);
        for (int i = 0; i < 9; i++) {
            controller.run();
        }
        assertEquals(unitGroups.size(), 0);
        assertEquals(controller.run(), GameController.STATUS_PLAYER_WIN);
    }

    
}
