/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import controller.inits.TestPlayerLooseInit;
import events.EventStorage;
import java.util.List;
import model.Point;
import model.towers.Tower;
import model.units.Unit;
import model.units.UnitGroup;
import events.controller.TowerReadyObserver;
import model.towers.AbstractBattleTower;
import org.hamcrest.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author miha
 */
public class ControllerTest {

    private GameController controller;
    private List<UnitGroup> unitGroups;
    private List<Unit> units;
    private List<Tower> towers;
    private Unit unit;
    private AbstractBattleTower tower;

    public ControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
        controller = GameController.initTestMode(new TestPlayerLooseInit());

        units = controller.getUnitsController().getUnitSequence().getUnitsGroups().get(0).getUnits();
        unitGroups = controller.getUnitsController().getUnitSequence().getUnitsGroups();
        towers = controller.getTowersController().getTowersMap().getTowers();

        unit = units.get(0);
        tower = (AbstractBattleTower) towers.get(0);
    }

    @After
    public void tearDown() {
        controller.removeEvent(EventStorage.TOWER_READY);
        controller.removeEvent(EventStorage.TOWER_HIT);
        tower.getBullets().clear();
        tower.setCooldown(0);
    }

    @Test
    public void initializeTest() {

        assertEquals(towers.size(), 1);
        assertEquals(unitGroups.size(), 2);

        assertEquals(units.size(), 1);

        assertEquals(unit.getHealth(), 500);

        assertEquals(controller.getLevel().getLifes(), 2);
        assertEquals(controller.getLevel().getStartGoldAmount(), 30);

        assertEquals(controller.getUser().getGold(), controller.getLevel().getStartGoldAmount());

    }

    @Test
    public void moovingTest() {
        assertEquals(unit.getPoint(), new Point(0, 0));
        for (int i = 0; i < 3; i++) {
            controller.run();
        }
        assertEquals(unit.getPoint(), new Point(-3*unit.getSpeed(), 0));

    }

    @Test
    public void nextNodeTest() {
        assertEquals(unit.getPoint(), new Point(0, 0));
        for (int i = 0; i < 99; i++) {
            controller.run();
        }
        assertEquals(unit.getPoint(), new Point(-20, 0));
        controller.run();

        assertEquals(unit.getPoint(), new Point(-20, -0.2f));
    }

    @Test
    public void shootingTest() {
        int health = unit.getHealth();
        assertEquals(tower.checkDistance(units.get(0).getPoint()), true);
        assertEquals(tower.getCooldown(), 0.0, 0.0001);
        controller.run();
        assertEquals(tower.getCooldown(), 9);
        assertEquals(tower.getBullets().size(), 1);
        
        controller.run();
        controller.run();
        assertEquals(tower.getCooldown(), 7, 0.0001);
        assertEquals(unit.getHealth(), health - tower.getDamage());
        for (int i = 0; i < 8; i++) {
            controller.run();
        }
        assertEquals(tower.getCooldown(), 9);
        assertEquals(tower.getBullets().size(), 1);
        controller.run();
        controller.run();
        controller.run();
        controller.run();
        assertEquals(tower.getCooldown(), 5, 0.0001);
        assertEquals(unit.getHealth(), health - 2*tower.getDamage());

    }

    @Test
    public void nextWaveTest() {
        assertEquals(unitGroups.get(1).isOnMap(), false);
        for (int i = 0; i < 197; i++) {
            controller.run();
        }
        assertEquals(unitGroups.get(1).isOnMap(), false);
        controller.run();
        assertEquals(unitGroups.get(0).isOnMap(), false);
        controller.run();
        assertEquals(unitGroups.get(0).isOnMap(), true);
    }

    @Test
    public void looseLifesTest() {
        assertEquals(controller.getUser().getLifes(), 2);
        for (int i = 0; i < 205; i++) {
            controller.run();
        }
        assertEquals(controller.getUser().getLifes(), 1);
        for (int i = 0; i < 189; i++) {
            controller.run();
        }

        assertEquals(controller.getUser().getLifes(), 1);
        assertEquals(controller.run(), GameController.STATUS_PLAYER_LOOSE);
        assertEquals(controller.getUser().getLifes(), 0);
    }

    @Test
    public void towerCreateAndRemoveTest() {
        assertEquals(unitGroups.size(), 2);
        assertEquals(unit.getHealth(), 500);
        Tower newTower = new AbstractBattleTowerImpl(new Point(1, 2));

        assertFalse(controller.getTowersController().checkTowerBuilding(newTower, controller.getUser().getGold()));
        newTower = new AbstractBattleTowerImpl(new Point(2, 2));
        assertFalse(controller.getTowersController().checkTowerBuilding(newTower, 5));
        assertTrue(controller.getTowersController().checkTowerBuilding(newTower, controller.getUser().getGold()));

        controller.getTowersController().addTower(newTower);
        assertEquals(controller.getUser().getGold(), controller.getLevel().getStartGoldAmount() - newTower.getCost());

        controller.run();
        controller.run();
        controller.run();
        assertEquals(unit.getHealth(), 480);

        controller.run();
        controller.run();
        assertEquals(unit.getHealth(), 470);

        controller.getTowersController().removeTower(newTower);
        assertFalse(towers.contains(newTower));
        assertEquals(controller.getUser().getGold(), controller.getLevel().getStartGoldAmount() - newTower.getCost() / 2);
        controller.run();
        assertEquals(unit.getHealth(), 470);

        for (int i = 0; i < 9; i++) {
            controller.run();
        }

        assertEquals(unit.getHealth(), 460);
    }

    private static class AbstractBattleTowerImpl extends AbstractBattleTower {

        private static final float radius = 30;
        private static final int damage = 10;
        private static final int cooldawn = 1;

        public AbstractBattleTowerImpl(Point point) {
            super(point, radius, 0, 10, damage, "", "");
        }

        @Override
        public UnitGroup targetHit(Unit target, UnitGroup units) {
            target.makeDamage(super.getDamage());

            return new UnitGroup(false, target);
        }
    }
}
