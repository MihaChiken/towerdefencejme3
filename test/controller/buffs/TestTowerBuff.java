/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.buffs;

import com.sun.corba.se.spi.activation._ActivatorImplBase;
import model.towers.buffs.AbstractTowerTimeBuff;

/**
 *
 * @author miha
 */
public class TestTowerBuff extends AbstractTowerTimeBuff {

    private static final boolean DEBUFF = false;
    private static final int TIME = 20;

    public TestTowerBuff() {
        super(DEBUFF, TIME);
    }

    @Override
    public int setDamage(int damage) {
        return damage + 1;
    }

    @Override
    public int setCooldown(int cooldown) {
        return cooldown - 1;
    }

    @Override
    public float setRadius(Float radius) {
        return radius + 2;
    }
}
