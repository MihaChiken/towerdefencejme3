/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.buffs;

import model.units.buffs.AbstractUnitsTimeBuff;

/**
 *
 * @author miha
 */
public class TestUnitBuff extends AbstractUnitsTimeBuff {

    private static final boolean DEBUFF = true;
    private static final int TIME = 10;

    public TestUnitBuff() {
        super(DEBUFF, TIME);
    }

    @Override
    public int setHealth(int health) {
        return health - 2;
    }

    @Override
    public float setSpeed(float speed) {
        return speed - 0.1f;
    }
}
