/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller.inits;

import controller.initializer.Initializer;
import java.util.ArrayList;
import java.util.List;
import model.Level;
import model.NodeChain;
import model.Point;
import model.towers.AbstractBattleTower;
import model.towers.TowersMap;
import model.units.AbstractUnit;
import model.units.Unit;
import model.units.UnitGroup;
import model.units.UnitSequence;

/**
 *
 * @author miha
 */
public class TestPlayerWinInit implements Initializer {

    @Override
    public void init(TowersMap towersMap, UnitSequence unitSequence, Level level) {
        towersMap.getTowers().clear();
        towersMap.getTowers().add(new AbstractBattleTowerImpl(new Point(1, 2)));

        List<Unit> wave1 = new ArrayList<Unit>();
        List<Unit> wave2 = new ArrayList<Unit>();

        wave1.add(new AbstractUnit(new Point(0, 0), 60, 10, 0.2f,0));
        wave2.add(new AbstractUnit(new Point(0, 0), 60, 10, 0.2f,0));

        unitSequence.getUnitsGroups().clear();
        unitSequence.getUnitsGroups().add(new UnitGroup(true, wave1));
        unitSequence.getUnitsGroups().add(new UnitGroup(false, wave2));

        NodeChain nodeChain = level.getNodeChain();
        nodeChain.addNode(new Point(0, 0));
        nodeChain.addNode(new Point(-20, 0));
        nodeChain.addNode(new Point(-20, -20));

        level.setLifes(2);
    }

    private static class AbstractBattleTowerImpl extends AbstractBattleTower {

        private static final float radius = 30;
        private static final int damage = 10;
        private static final int cooldawn = 1;

        public AbstractBattleTowerImpl(Point point) {
            super(point, radius, 0, 2, damage, "", "");
        }

       @Override
        public UnitGroup targetHit(Unit target, UnitGroup units) {
            target.makeDamage(super.getDamage());
        
            return new UnitGroup(false, target);
        }
    }
}
